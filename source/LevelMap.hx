package;

import flixel.FlxObject;
import flixel.util.FlxAxes;
import flixel.tile.FlxTilemap;
import flixel.tile.FlxTile;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledTileLayer;
import flixel.addons.editors.tiled.TiledTileSet;

class LevelMap extends TiledMap
{
    public var tileSet:TiledTileSet;
    public var tileMap:FlxTilemap;

    override public function new(tileLevel:Dynamic)
    {
        super(tileLevel);

        var tileLayer:TiledTileLayer = cast getLayer("bricks");
        tileSet = tilesets["tiles"];

        var path:String = "assets/images/tiles.png";

        tileMap = new FlxTilemap();
        tileMap.loadMapFromArray(
                tileLayer.tileArray,
                width, height, path,
                30, 15,
                OFF, tileSet.firstGID, 1, 1
        );
        for (tIndex in tileSet.firstGID...tileSet.numTiles)
        {
            tileMap.setTileProperties(tIndex, FlxObject.ANY, tileCallback, Ball);
        }

        tileMap.screenCenter(FlxAxes.X);
    }
    private function tileCallback(t:FlxObject, ball:FlxObject):Void
    {
        var tile:FlxTile = cast t;
        tileMap.setTileByIndex(tile.mapIndex, 0);
    }
    public static function collide(t:FlxObject, b:FlxObject):Bool
    {
        var tile:FlxTile = cast t;
        var ball:Ball = cast b;

        tile.elasticity = 1;
        tile.immovable = true;

        FlxObject.separate(tile, ball);

        return true;
    }
}