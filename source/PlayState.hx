package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.util.FlxCollision;

class PlayState extends FlxState
{
	var ball:Ball;
	var bricks:LevelMap;
	var walls:FlxGroup;

	override public function create():Void
	{
		super.create();

		ball = new Ball(FlxG.width/2, FlxG.height/1.5);
		bricks = new LevelMap("assets/data/example.tmx");

		walls = FlxCollision.createCameraWall(FlxG.camera, false, 1, false);
		
		add(bricks.tileMap);
		add(ball);
		add(walls);
		
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);

		FlxG.collide(ball, walls);

		bricks.tileMap.overlapsWithCallback(ball, LevelMap.collide);
	}
}
