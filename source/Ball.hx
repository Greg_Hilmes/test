package;

import flixel.FlxSprite;
import flixel.util.FlxColor;

using flixel.util.FlxSpriteUtil;

class Ball extends FlxSprite
{
    public var powerups = {};
    override public function new(X:Float, Y:Float):Void
    {
        super(X, Y);

        makeGraphic(8, 8, FlxColor.TRANSPARENT);
        drawCircle(4, 4, 4);
        velocity.set(0, 300);
        elasticity = 1;
    }
}